# Health Center API

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

Backend for health-center : health-related tips (sport, nutrition, sleep ...)

## Getting Started

### Install dependencies

    npm install

### Environment Variables

Create a **_.env_** file and populate it with environment varaibles values.
Available environment variables are listed in **_.env-sample_**

### Start dev server

    npm run dev
