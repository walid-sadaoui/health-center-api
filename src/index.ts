import express from "express";
import logger from "morgan";
import cors from "cors";
import helmet from "helmet";

const app = express();
app.use(helmet());
app.use(logger("dev"));
app.use(cors());
app.use(express.json());

export default app;
