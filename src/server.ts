import http from "http";
import * as dotenv from "dotenv";
import app from "./index";

dotenv.config();

const PORT: number = parseInt(process.env.PORT as string, 10) || 3000;
const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
